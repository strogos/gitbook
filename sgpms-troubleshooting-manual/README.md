#  SGPMS Troubleshooting Manual  

<br />

![PMScard](content/images/PMS-card.PNG "Amp card illustriation")  


**Introduction**
> The basic functionality of the Amplifier card (AMP) in the Steering Gear (SG) control system is to compare rudder order and rudder feedback signals and thus generate steering signals.  
> 
> A subsystem on this card - the Performance Monitoring System (PMS), is able to monitor and log the performance and health of the Steering Gear. This document will attempt to tackle the functionality and potential of this system from both a technical and commercial perspective. It will also provide an overview, ’know-how’ and thematic cheat-list for analyzing SG vessel data produced by the logging functionality of the card.  



 
   
